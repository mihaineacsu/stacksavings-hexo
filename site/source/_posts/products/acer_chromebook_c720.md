title: Acer Chromebook C720 
imgfolder: acer_chromebook_c720
price: 160
productcartname: Chromebook+Acer+C720+Laptop
allowcomments: true
pagetype: product
amazonproductlink: http://www.amazon.com/Acer-C720-Chromebook-11-6-Inch-2GB/dp/B00FNPD1VW/ref=aag_m_pw_dp?ie=UTF8&m=A14VYY2FQ2ELZA
---
  
- 11.6 inch (1366x768) display, 16:9 aspect ratio
- 0.75 inches thin - 2.76 lbs/ 1.25kg
- Up to 8.5 hours of active use
- New Intel Celeron processor
- 100 GB Google Drive Cloud Storage2&nbsp;with 16GB Solid State Drive
- 60-day free trial with Google Play Music All Access
- Built-in dual band Wi-Fi 802.11 a/b/g/n
- VGA Camera
- 1x USB 3.0, 1x USB 2.0
- Full size HDMI Port
- Bluetooth 4.0 Compatible
            