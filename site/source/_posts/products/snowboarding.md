title: Top 10 Snowboarding Resorts
imgfolder: snowboarding
type: toptenlist
items: NorthStar|Whistler|Park City
allowcomments:true
pagetype: product
---
NorthStar Resort in the Sierra Nevada Mountains in California starts our list off at #10.

The snowboarding season is here. Ring in the new year by visiting local winter resots. Winter storms have brought plenty of snow to most ski resorts. Save on the latest snowboarding lift ticket deals.
