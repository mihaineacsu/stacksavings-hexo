title: Contractor Project Setup Info
pagetype: contractor
---
<b>Create account on Cloud9:</b>

<a class="analytics" href="http://c9.io">c9.io</a>

Then share your user id with us and we will assign you to a workspace
<br/>
<br/>

<b>If you want to download the project locally, you can pull it from BitBucket:</b>

git@bitbucket.org:rickdsac/stacksavings-hexo.git

If you download locally, you will need to Open new workspace, at root level, in terminal, run:

npm install -g hexo

(see hexo.io for more info on Hexo)
<br/>
<br/>

<b>To Run the project, in the terminal:</b>

cd store
hexo server
