title: Software Developer Intro
pagetype: contractor
---
You will be provided with a Cloud9 Workspace ( <a href="http://c9.io">c9.io</a> ) that is already setup and configured for you, in order to minimize project setup time.

After opening the workspace, you should have a terminal that is in the project workspace root directory. From here, go to Hexo project:

    cd site

Then, all it takes to start the project is:

    hexo server
    
The project will be up and running at:

    http://your-c9-workspace-name-rickdsac.c9.io
    
(replace, your-c9-workspace-name, with the name provided to you as your Cloud9 workspace)

