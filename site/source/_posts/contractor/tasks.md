title: Open Contract Tasks
pagetype: contractor
---
<b>Current Open Tasks:</b>
<i>Please apply on <a class="analytics" href="https://www.odesk.com/signup/user-type">odesk.com</a</i>
<br/>

- <a class="analytics" href="https://www.odesk.com/jobs/Node-Javascript-implement-Contact-Form_~018b7ad5b90da26bda">Node.js, Javascript, implement Contact Form</a>

- <a class="analytics" href="https://www.odesk.com/jobs/~0128f90cfbbc66d013">Mobile web app layout re-design, ongoing tasks</a>

- <a class="analytics" href="https://www.odesk.com/jobs/Node-Javascript-implement-Amazon-Product-Search-API-results-javascript_~~264326a8ccadf1d6">The project is to integrate a search feature that calls Amazon Products API with our existing Node.js framework.</a>

<br/>


<b>About Us:</b> StackSavings.com is a US based eCommerce website that was recently launched. Our platform focuses on web-based mobile users who access our web application from smartphones. We are looking to work with experienced JavaScript programmers for long-term projects.

<b>Work Environment:</b> We are able to provide a new workspace for each contractor, on the Clou9 IDE: http://c9.io

This minimizes project setup time, you have the option to download the code locally as well but we are not able to help with local project setup, although it should be fairly simple if you are experienced with Node.js.



<b>View a demo worksapce at:</b>

<a class="analytics" href="https://ide.c9.io/rickdsac/clone9">ide.c9.io/rickdsac/clone9</a>