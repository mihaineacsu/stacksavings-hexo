title: Privacy Policy
---
All data is stored securely and is confidential. We do not use customer data for marketing purposes or sell or provide data in any way to other companies.