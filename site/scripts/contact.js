var bodyParser = require('body-parser'),
	filter = hexo.extend.filter,
	fs = require('fs');


filter.register('server_middleware', function (app) {
	app.use(bodyParser.urlencoded({ extended: false }));

    app.use('/contact/', function(request, response){
    	if (request.method === 'POST'){
    		var formBody = request.body;

    		// you could add extra information, such as date of submission
			fs.appendFile('contact.json', JSON.stringify(formBody, null, 4), function(err) {
			    if(err) {
			        console.log(err);
			    } else {
		        console.log('Appended form body to contact.json');
				}
			}); 

			response.writeHead(302, {
			  'Location': '/contact/index.html'
			  // add other headers here,
			  // you could send the user to a new route where he gets a
			  // thank you message
			});
			response.end();
    	}
	});
});
